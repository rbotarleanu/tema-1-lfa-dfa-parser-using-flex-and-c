build:
	flex lexer.lex
	gcc lex.yy.c alphabet.c DFA.c list.c -lfl
clean:
	rm -f lex.yy.c a.out *.out* *sorted*
run:	
	./a.out
	
