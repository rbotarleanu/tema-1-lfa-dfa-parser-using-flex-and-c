/* BOTARLEANU Robert-Mihai 331CB
 * Header file that is used to describe a deterministic finite automaton,
 * it's functions and checks the DFA's structure for any errors.
 */

#ifndef DFA_HEADER_GUARD
#define DFA_HEADER_GUARD

#include <stdio.h>					// standard I/O
#include "list.h"					// simple linked list for data sets
#include "alphabet.h"				// structures the alphabet of the DFA

#define MAX_STATE_NAME_SIZE 100	
#define MAX_LINE_SIZE		50000

// typedef for a state value as a char array of fixed length
typedef struct st { 
	char string[MAX_STATE_NAME_SIZE];
} st;

// struct for a transition relation
typedef struct transition {
	st origin;					// origin state
	st destination;				// destination state
	unsigned char symbol;		// transition symbol
} transition;					// (origin, symbol) -> destination
	
// struct for the deterministic finite automaton
typedef struct DFA {
	list* 		states;			// the states of the automaton
	alph*		alphabet;		// the alphabet of the language it accepts
	list* 		transitions;	// the transition relations
	st 			initial_state;	// the initial state of the automaton
	list* 		final_states;	// the final state[s] of the automaton
} DFA;

// creates the DFA, allocating all necessary memory
DFA* DFA_create();
// destroys the DFA, deallocating all used memory
void DFA_destroy(DFA* automaton);

/* runs the DFA against an input file, checking all 
 * substrings of the text if they are accepted by the DFA
 */
void DFA_run_against_file(DFA* automaton, const char *file_name);
// checks the DFA for any semantic errors
int DFA_check_for_semantic_errors(DFA* automaton);

#endif