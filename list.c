/* BOTARLEANU Robert-Mihai 331CB
 *	Simple linked list, as described in the header.
 */

#include "list.h"

// create a cell with the given data
list* make_cell(void *el) {
	// allocate memory for the cell
	list* cell = (list*) calloc(1, sizeof(list));
	cell->i = el;	// set the data
	cell->next = NULL;	// no next link
	return cell;	// return the reference
}

void list_add(list** l, int pos, void *el) {
	int 			i;
	list* 			it;
	list*			aux;
	// make a cell
	list* cell = make_cell(el);
	if(l == NULL) {   // new list
		*l = cell; 
		return; 
	}	
	if(pos == 0) { // add to first position
		cell->next = *l;
		*l = cell;
		return;
	}
	// find the previous position in the list
	for(i = 0, it = *l; i < pos - 1 && it->next != NULL; ++i)
		it = it->next;
	aux = it->next;
	it->next = cell; 	// put the cell
	cell->next = aux;
}

void list_remove(list** l, int pos) {
	list* cell = *l;
	list* aux = *l;
	if(pos == 0) { *l = (*l)->next; } // destroy the first cell
	else { // find the previous cell in the list
		while(--pos && aux != NULL) aux = aux->next;
		cell = aux->next;	// redirect pointers to skip it
		aux->next = cell->next;
	}	
	free(cell); // destroy the cell
}

void* list_get(list *l, int pos) {
	++pos;	// 0-based numbering
	while(--pos && l != NULL) {
		l = l->next;	// go to the cell
	}
	if(l == NULL) return NULL;	// position too high
	return l->i;	// return the data
}

void list_destroy(list **l) {
	while(*l != NULL) {	// destroy each cell
		list* aux = *l;
		*l = (*l)->next;
		free(aux);
	}
}
