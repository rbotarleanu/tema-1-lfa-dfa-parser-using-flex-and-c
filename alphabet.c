/* BOTARLEANU Robert-Mihai 331CB
 * Source file for the alphabet, as described in the header
 */

#include "alphabet.h"

alph* alphabet_create() {
	// allocate memory for the data structure
	alph* alphabet = (alph*) calloc(sizeof(alph), 1);
	// initially, there are no symbols in the alphabet
	memset(alphabet->symbols, 0, ASCII_SIZE);
	return alphabet;	// return the reference
}

void  alphabet_add_symbol(alph* a, unsigned char symbol) {
	if(a == NULL) return;		// null pointer
	a->symbols[symbol] |= 1;	// set symbol in alphabet
}

char  alphabet_has_symbol(alph* a, unsigned char symbol) {
	if(a == NULL) return 0;		// null pointer
	return a->symbols[symbol] != 0;	// check if symbol is in alphabet
}

void  alphabet_remove_symbol(alph* a, unsigned char symbol) {
	if(a == NULL) return;		// null pointer
	a->symbols[symbol] &= 0;	// unset symbol in alphabet
}

void  alphabet_destroy(alph* a) {
	if(a == NULL) return;		// null pointer
	free(a);					// deallocate memory for the alphabet
}



