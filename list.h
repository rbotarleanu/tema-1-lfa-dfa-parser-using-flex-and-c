/* BOTARLEANU Robert-Mihai 331CB
 * Header file for a simple linked list with cells that have 
 * a generic value, given by a void pointer.
 */
#ifndef LIST_HEADER_GUARD
#define LIST_HEADER_GUARD

#include <stdlib.h>	// memory managament

/* the simple-linked list struct */
typedef struct list {
	void *i;				// data is generic
	struct list* next;		// next cell
} list;

// add an element to the list, create a new one if empty
void list_add(list** l, int pos, void *el);
// remove an element from the list
void list_remove(list** l, int pos);
// get an element at the specified position from the list
void* list_get(list *l, int pos);
// destroy the list, deallocating all used (but NOT for the cell data)
void list_destroy(list** l);

#endif