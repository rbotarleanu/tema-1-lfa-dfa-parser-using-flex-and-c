/*	BOTARLEANU Robert-Mihai 331CB
 *	Source file as described in the header.
 */

#include "DFA.h"

DFA* DFA_create() {
	// allocate memory for the automaton
	DFA* automaton = (DFA*) calloc(sizeof(DFA), 1);
	// create the alphabet
	automaton->alphabet = alphabet_create();
	return automaton;	// return the reference
}

void DFA_destroy(DFA* automaton) {
	if(automaton == NULL) return; 	// null reference
	alphabet_destroy(automaton->alphabet);		// deallocate the alphabet
	list_destroy(&automaton->states);			// deallocate the states
	list_destroy(&automaton->final_states);		// deallocate the final states
	list_destroy(&automaton->transitions);		// deallocate the transitions
	free(automaton);							// deallocate the DFA
}

// finds a transition from the curent state with the given symbol
int find_transition(DFA* automaton, char crt_state[MAX_STATE_NAME_SIZE],
		unsigned char symbol) {
	// list iterator
	list* aux = automaton->transitions;
	while(aux != NULL) {
		// the transition datatype reference
		transition* t = (transition*) aux->i;
		if(!strcmp(crt_state, t->origin.string)
			&& t->symbol == symbol) {	// the transition matches 
			// update the current state
			memcpy(crt_state, t->destination.string, 
				strlen(t->destination.string));
			// add string terminator
			crt_state[strlen(t->destination.string)] = '\0';
			return 0;		// success
		}
		aux = aux->next; // check the next transition
	}
	return -1;	// failure
}

// returns 1 if the state is final
int state_is_final(DFA* automaton, char crt_state[MAX_STATE_NAME_SIZE]) {
	// final states iterator
	list *aux = automaton->final_states;
	while(aux != NULL) { // find the current state in the final state list
		if(!strcmp(crt_state, (char*) aux->i))
			return 1;	// is final
		aux = aux->next;
	}
	return 0;	// state is not in final state list, it is not final
}

// runs the DFA with the given substring
int DFA_run_with_substring(DFA* automaton, char *string) {
	// the current state
	char		 crt_state[MAX_STATE_NAME_SIZE];
	// DFA starts from the intial state
	memcpy(&crt_state, automaton->initial_state.string, 
		strlen(automaton->initial_state.string));
	crt_state[strlen(automaton->initial_state.string)]='\0';
	// go through each symbol of the substring
	char *c = string;
	while(*c != '\n' && *c != '\0') {
		// invalid symbol, semantic error(should NOT happen)
		if(!alphabet_has_symbol(automaton->alphabet, *c)) {
			return 0;
		}
		// find a transition for this state and symbol and change state
		int r = find_transition(automaton, crt_state, *c);
		if(r == -1) {
			return -1;	// couldn't transition
		}
		++c;	// consume the substring 
	}
	// DFA is done going through the substring
	// check if we arrived at a final state
	if(state_is_final(automaton, crt_state)) {
		// if yes, then the substring is accepted
		// by the automaton and we print it		
		printf("%s\n", string);	
		return 1;
	}
	return 0;	// substring isn't accepted
}

// generates all substrings of a given string and runs the DFA for each
void DFA_generate_substrings_and_run(DFA* automaton, char *string) {
	int len, start;					// substring length and start position
	char substr[MAX_LINE_SIZE];		// the substring
	for(len = 1; len < strlen(string) + 1; ++len) {	// len 1..size(string)
		for(start = 0; start < strlen(string) - len + 1; ++start) {
			// string[ start .. start + len ]
			memcpy(substr, string + start, len);
			substr[len] = '\0';
			// run the DFA with the substring
			DFA_run_with_substring(automaton, substr);
		}
	}
	
}

/* Runs the DFA with the given file to determine all substrings of the text
 * that are accepted by the automaton
 */
void DFA_run_against_file(DFA* automaton, const char *file_name) {
	FILE* fin = fopen(file_name, "r");	// open file
	if(fin == NULL) {	// file not found
		printf("No text file found\n");
		return;
	}
	/* construct the separator list */
	unsigned char separators[ASCII_SIZE];
	int sep_crt = 0;
	int i = 0;
	for(i = 0; i < ASCII_SIZE; ++i) {
		// the separator list is given by all symbols not in the alphabet
		if(!alphabet_has_symbol(automaton->alphabet, (unsigned char) i)) {
			separators[sep_crt++] = (unsigned char) i;
		}
	}
	// null terminate the separator list
	separators[sep_crt] = '\0';
	// read line by line from the input file
	char line[MAX_LINE_SIZE];
	while(fgets(line, MAX_LINE_SIZE, fin) != NULL) {
		if(line[strlen(line)-1] == '\n') { // strip \n
			line[strlen(line)-1]='\0';
		}
		// split the line
		char* tok = strtok(line, (char *) separators);
		while(tok != NULL) { // generate all substrings and run the DFA
			DFA_generate_substrings_and_run(automaton, tok);
			tok = strtok(NULL, (char *) separators); // go to next token
		}
	}
}

// checks if given state is in the automaton's state set
int DFA_check_if_state_is_in_state_set(DFA* automaton, const char *state) {
	list* aux = automaton->states;	// states iterator
	while(aux != NULL) { 
		char* _state = (char*) aux->i;
		// check if the state is found
		if(!strcmp(state, _state)) return 1;
		aux = aux->next;
	}
	return 0; // state not found in state list -> semantic error
}

// checks all transitions of a given state for semantic errors
int DFA_check_transitions_for_state(DFA* automaton, const char *state,
	int transitions_per_state) {
	int 			transitions = 0; // count transitions
	list			*aux;		
	list			*aux1;		
	transition 		*t1;
	transition      *t2;
	for(aux = automaton->transitions; aux != NULL; aux = aux->next) {
		t1 = (transition*) aux->i;
		if(strcmp(t1->origin.string, state)) continue;	// not this state
		char correct = 1; // no errors must be found
		// check if origin state is in the state list
		correct &= DFA_check_if_state_is_in_state_set(automaton, 
			t1->origin.string);
		// check if destination state is in the state list
		correct &= DFA_check_if_state_is_in_state_set(automaton, 
			t1->destination.string);
		// check if there any semantic errors so far
		if(!correct) return 0;
		// check for duplicates
		for(aux1 = automaton->transitions; aux1 != NULL; aux1 = aux1->next) {
			if(aux == aux1) continue;	// same pointer
			t2 = aux1->i;
			if(t1 == t2) continue;
			if(strcmp(t2->origin.string, state)) continue; //not this state
			if(t2->symbol == t1->symbol &&
			   !strcmp(t2->destination.string, t1->destination.string)) {
				return 0;	// duplicate transition
			}
		}
		++transitions;	// count transitions
	}
	// the number of transitions for each state must be equal to the number 
	// of symbols in the alphabet
	return transitions == transitions_per_state;	 
}

// checks the DFA for semantic errors
int DFA_check_for_semantic_errors(DFA* automaton) {
	int 	transitions_per_state = 0;
	int 	i;
	list 	*aux;
	// find the number of transitions
	for(i = 0; i < ASCII_SIZE; ++i)
		if(alphabet_has_symbol(automaton->alphabet, (unsigned char) i))
			++transitions_per_state;
	// check if the initial state is in the state set
	if(!DFA_check_if_state_is_in_state_set(automaton, automaton->initial_state.string))
		return 1;
	// check if the number of transitions is correct for each state
	for(aux = automaton->states; aux != NULL; aux = aux->next)
		if(!DFA_check_transitions_for_state(automaton, (char *) aux->i, transitions_per_state))
			return 1;
	// check if the final states are in the state set
	for(aux = automaton->final_states; aux != NULL; aux = aux->next)
		if(!DFA_check_if_state_is_in_state_set(automaton, (char *) aux->i)) {
			return 1;
		}

	// automaton is correct
	return 0;
}