/* BOTARLEANU Robert-Mihai 331CB 
 * Header file that is used to describe a simple alphabet implementation.
 * The alphabet is memorized as a lookup table, and the module offers
 * various functions for interacting with the alphabet.
 */

#ifndef ALPHABET_HEADER_GUARD
#define ALPHABET_HEADER_GUARD

#include <string.h>		// memset
#include <stdlib.h>		// memory management

#define ASCII_SIZE		256

/* typedef for the alphabet as a lookup table, if
 * symbol[i] is set, then character >i< appears in the alphabet
 */
typedef struct alphabet {
	unsigned char symbols[ASCII_SIZE];
} alph;

// creates the alphabet, allocating all necessary memory
alph* alphabet_create();
/* adds a symbol to the alphabet, seting the coresponding 
 * position in the lookup table as 1
 */
void  alphabet_add_symbol(alph* a, unsigned char symbol);
// checks if the alphabet has the given symbol
char  alphabet_has_symbol(alph* a, unsigned char symbol);
// removes a symbol from the alphabet
void  alphabet_remove_symbol(alph* a, unsigned char symbol);
// destroys the alphabet, deallocating all used memory
void  alphabet_destroy(alph* a);

#endif