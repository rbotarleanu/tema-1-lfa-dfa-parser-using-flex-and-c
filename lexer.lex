%top {
	#include <stdio.h>
	#include <math.h>
	#include <string.h>		
	#include "DFA.h"

	#define STATES 			0
	#define FINAL_STATES 	1
	#define lastchar yytext[yyleng - 1]
	#define YY_BUF_SIZE 66000

	DFA* automaton;				/* the DFA */
	int context = STATES;     	/* state context */
	transition* t;				/* current transition */
	int crt_state = 0;
	int crt_transition = 0;
	int syntax_error = 0;		/* syntax error */
	int semantic_error = 0;		/* semantic error */
}

/* smybol, alphabet and string */
lowercase_letter 	[a-f]|[f-z]
uppercase_letter 	[A-F]|[F-Z]
digit 			 	[0-9]
other "!"|"#"|"$"|"%"|"&"|"-"|"."|"/"|":"|";"|"<"|">"|"="|"@"|"["|"]"|"?"|"+"|"^"|"'"|"`"|"\""|"_"|"*"|"~"|"|"
symbol {uppercase_letter}|{lowercase_letter}|{digit}|{other}
word   {symbol}+

/* Deterministic finite automaton */
name   ({uppercase_letter}|{lowercase_letter}|{digit}|"_")+
state  {name}

/* start and finish parsing the DFA */
%s DFA_START  DFA_END
/* lexer states for parsing the DFA states */
%s STATES_START STATES_SEP STATES_SYMB STATES_END
/* lexer states for parsing the DFA alphabet */
%s ALPH_START ALPH_SEP ALPH_SYMB ALPH_END
/* lexer states for parsing the DFA transitions */
%s TRANSITIONS_SYMB TRANSITIONS_SOURCE TRANSITIONS_SYMBOL_SEP TRANSITIONS_DESTINATION_SYMBOL
%s TRANSITIONS_SYMBOL TRANSITIONS_DESTINATION_SEP TRANSITIONS_SEP TRANSITIONS_START
/* initial lexer state and the transition to the final states */
%s INITIAL_STATE TRANSITION_TO_FINAL_STATES


%%
[ \t\r\n ] 				// consume whitespaces

<INITIAL>"({" { /* first read the states list after the "(" */
	BEGIN(STATES_SYMB);	
}

<STATES_SYMB>{	/* read the first state */
	{state} {
		// allocate memory for the state string
		char *aux = (char*) calloc(sizeof(char), MAX_STATE_NAME_SIZE);
		memcpy(aux, yytext, strlen(yytext));
		if(context == STATES)	// check which states we are reading
			list_add(&automaton->states, crt_state++, aux);	
		else 
			list_add(&automaton->final_states, crt_state++, aux);
		BEGIN(STATES_SEP);	// go to the separator
	}
	"}" {	
		// no final states
		if(context == FINAL_STATES) BEGIN(DFA_END);
		else syntax_error = 1;	// we are in the state list! it cannot be void
	}
}

<STATES_SEP>{ /* check the separator */
	","	 BEGIN(STATES_SYMB); 	// read another state
	"}"	 {
			// if we finished reading the states
			if(context == STATES)	
				BEGIN(STATES_END);   // go to the alphabet if in states
			else 
				BEGIN(DFA_END);	 // finish reading the DFA if in final states
		}
}

<STATES_END>",{" { /* start reading the alphabet */ 
	if(context == STATES)
		BEGIN(ALPH_SYMB); 
	else 
		BEGIN(DFA_END);	
}

<ALPH_SYMB>{symbol} { /* read the first alphabet smybol */
	/* add symbol to alphabet */
	alphabet_add_symbol(automaton->alphabet, lastchar);
	/* go to next state */
	BEGIN(ALPH_SEP);
}
<ALPH_SEP>{ /* check the separator */
	"," BEGIN(ALPH_SYMB);	// read another symbol
	"}" BEGIN(ALPH_END);	// finish reading the alphabet
}

<ALPH_END>",(" { /* alphabet has ended, start reading transitions */
	BEGIN(TRANSITIONS_START);
}

<TRANSITIONS_START>"d(" {	
	// read the general transition format and allocate a new transition 
	t = (transition*) calloc(1, sizeof(transition));
	BEGIN(TRANSITIONS_SOURCE);	// read the source
}

<TRANSITIONS_SOURCE>{state} {
	// memorize the source
	memcpy(&t->origin.string, yytext, strlen(yytext));
	BEGIN(TRANSITIONS_SYMBOL_SEP);	// read the separator
}

<TRANSITIONS_SYMBOL_SEP>"," {
	BEGIN(TRANSITIONS_SYMBOL);	// read the symbol
}

<TRANSITIONS_SYMBOL>{symbol} {
	// if the symbol is not in the alphabet, we have a semantic error
	if(!alphabet_has_symbol(automaton->alphabet, lastchar)) {
		semantic_error = 1;
		t->symbol = lastchar;
		BEGIN(TRANSITIONS_DESTINATION_SEP);
	} else {
		t->symbol = lastchar;
		BEGIN(TRANSITIONS_DESTINATION_SEP);	// read the destination symbols
	}
}

<TRANSITIONS_DESTINATION_SEP>")=" {
	BEGIN(TRANSITIONS_DESTINATION_SYMBOL);	// read the destination state
}

<TRANSITIONS_DESTINATION_SYMBOL>{state} {
	// save the transition
	memcpy(&t->destination.string, yytext, MAX_STATE_NAME_SIZE);
	list_add(&automaton->transitions, crt_transition++, t);
	/* go to the next separator */
	BEGIN(TRANSITIONS_SEP);
}

<TRANSITIONS_SEP>{	
	","  BEGIN(TRANSITIONS_START);  // more transitions
	")," BEGIN(INITIAL_STATE);		// read the initial state
}

<INITIAL_STATE>{state} {
	// save the initial state of the DFA
	memcpy(&automaton->initial_state, yytext, strlen(yytext));
	BEGIN(TRANSITION_TO_FINAL_STATES); // prepare to read the final states
}
<TRANSITION_TO_FINAL_STATES>",{" {
	context = FINAL_STATES;	// we are reading final states
	crt_state = 0;			// state counter
	BEGIN(STATES_SYMB);		// read states
}

<DFA_END>")" {
	/* done parsing the dfa with no issues */
}

.	syntax_error = 1; /* if the lexer falls back here, there is an error */


%%
void DFA_destroy_states() {
	// deallocate memory for all state names
	list* aux = automaton->states;
	while(aux != NULL) {
	 	free((char*) aux->i);
		aux = aux->next;
	}
	// deallocate memory for all final state names
	aux = automaton->final_states;
	while(aux != NULL) {
	 	free((char*) aux->i);
		aux = aux->next;
	}
}

void DFA_destroy_transitions() {
	// deallocate memory for all transitions
	list *aux = automaton->transitions;
	while(aux != NULL) {
		free((transition*) aux->i);
		aux = aux->next;
	}
}

int main(void) {
	// prepare an automaton
	automaton = DFA_create();
	
	// prepare the input file and run the lexer
 	FILE* f = fopen("input", "rt");
 	yyrestart(f);
	yylex();

	if(syntax_error) {	
		// check if the file has a syntax error
		printf("Syntax error\n");
		return 0;
	}
	
	if(semantic_error || DFA_check_for_semantic_errors(automaton)) {
		// check if the automaton has a semantic error
		printf("Semantic error\n");
		return 0;
	}

	/* run the automaton against the input file */
	DFA_run_against_file(automaton, "text");

	// deallocate all memory
	DFA_destroy_states();
	DFA_destroy_transitions();
	DFA_destroy(automaton);
	fclose(f);
	return 0;
}